# Whitelist Javascript Websites Add-on

This is a simple GNU licensed Mozilla plugin to add a button to the toolbar for user to toggle site specific JavaScript execution setting. The plugin will save site specific preferences using simple-storage API and can export settings into a text file. 

Preferences are stored according to the full host name, so different settings apply for host1.domain.com and host2.domain.com. The plugin does not check the domain names of embedded or linked javascript source files, so eg. commonly used libraries, or any libraries, like jUnit will be executed as long as the browser location domain is enabled.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This plugin will require at least Firefox version 38.0a1, that is
roughly something that has been released during or after 2015.

For development environment you will need GIT of course and decent
tools for JavaScript development together with Mozilla Add-on SDK.
Below you will find a sample transcript of a Brackets development
environment setup using recent Kali Linux Light distribution from the
moment of birth of the VM to the IDE launch.


```
apt-get install git curl phantomjs nodejs nodejs-legacy npm gconf-service gconf2-common libgconf-2-4 libpango1.0-0 libpangox-1.0-0 libpangoxft-1.0-0
npm install jpm --global
curl -OJ 'ftp://ftp.debian.org/debian/pool/main/libg/libgcrypt11/libgcrypt11_1.5.0-5+deb7u4_amd64.deb'
curl -OJL 'https://github.com/adobe/brackets/releases/download/release-1.10-prerelease-1/Brackets.Release.release-1.10-prerelease-1.64-bit.deb'
dpkg -i *.deb
git clone http://github.com/veto64/JavaScript-Whitelist
```

After the installation completes, open up the Brackets IDE via GNOME Menu (located under /opt/brackets), and open up the project folder to do edits. Finally compile the edits and test using Mozilla SDK.

```
cd ~/JavaScript-Whitelist
jpm xpi
jpm run
```

### Installing

Installing this Add-on into browser is done by the standard browser Add-on Manager. Please refer to Firefox documentation or addon.mozilla.org if you have any difficulties installing this add-on.

## Development Environment

For development environment, please refer to https://developer.mozilla.org/en-US/Add-ons/SDK/Tools/jpm#Installation

## Running the tests

There are no tests currently in this project. Please fee free to write such and submit back to the development team.

## Contributing

Please read [CONTRIBUTING.md](https://github.com/veto64/JavaScript-Whitelist) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

This project does not have specific version numbering in use yet. Current version number is found in package.json file and GIT includes only master branch. TODO: Add a release hierarchy to the GIT tree.

## Authors

* **Veto64** - *Initial work* - (https://github.com/veto64/JavaScript-Whitelist)

See also the list of [contributors](https://github.com/veto64/JavaScript-Whitelist) who participated in this project.

## License

This project is licensed under the GNU License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Warm breeze from the sea
* etc