var tabs     = require("sdk/tabs");
var url      = require("sdk/url")
var buttons  = require('sdk/ui/button/toggle');
var ss       = require("sdk/simple-storage");
var page_mod = require("sdk/page-mod");
var pref     = require("sdk/preferences/service");
var data     = require("sdk/self").data;
var sp       = require("sdk/simple-prefs");
var prefs = require("sdk/simple-prefs").prefs;
var {Cc, Ci} = require("chrome");
var msg = Cc["@mozilla.org/embedcomp/prompt-service;1"].getService(Ci.nsIPromptService);
const path = require('sdk/fs/path');
const file = require('sdk/io/file');
const desktop_path = require('sdk/system').pathFor('Desk');
var panel = require("sdk/panel").Panel({
  contentURL: data.url("whitelist.html"),
  contentScriptFile: data.url("whitelist.js")
});


var Main = {
 open_tabs : [],
 add_on_name: 'Javascript Whitelist',   
 js   : false,
 btn : false,
 black_state : { "label": "Blacklisted", "icon": {"16": "./black16.png","32": "./black32.png","64": "./black64.png"}},
 white_state : { "label": "Whitelisted", "icon": {"16": "./white16.png","32": "./white32.png","64": "./white64.png"}},
 button : buttons.ToggleButton({
    id: "whitelist",
    label: "Blacklisted",
    icon: {
      "16": "./black16.png",
      "32": "./black32.png",
      "64": "./black64.png"
    }
  }),

 init : function(){
   Main.button.on("click",function()
   {
     if (Main.button.label == "Blacklisted")
     {
       Main.button.state(Main.button, Main.white_state);
       Main.add_to_whitelist();
     }
     else
     {
       Main.button.state(Main.button, Main.black_state);
       Main.remove_from_whitelist();
     }
       Main.btn = true;
     tabs.activeTab.reload();
   });

   if(!ss.storage.whitelist)
   {
     ss.storage.whitelist = [];
   }

   tabs.on('activate', function(tab) {
     var host = url.URL(tab.url).host;
     if(Main.host != host && host != null)
     {
       Main.host = host;
       Main.set_js(false);
       Main.set_button_state();
     } 	 
   });

  page_mod.PageMod({
    contentScriptWhen: 'start', 
    include: ['*'],
      onAttach: function(worker){
      var current_host =  url.URL(tabs.activeTab.url).host;
      var host = url.URL(worker.tab.url).host;
      if(Main.host != host && host != null && current_host == host)
      {
        Main.host = host;
        Main.set_js(true);	     
        Main.set_button_state();
      }
      else if (Main.btn == true)
      {
        Main.host = host;	     	      
        Main.set_js(true);	      
      }
      Main.btn = false;	  	      
    }	  
  });

  },

  set_js : function(reload){
    var white_host =  Main.is_in_whitelist()
    if(white_host)
    {
      if(pref.get('javascript.enabled') != true)
      {	  
        pref.set('javascript.enabled', true);
        if(reload)
	{      
            Main.reload();
	}
      }	 
   }
   else
   {
     if(pref.get('javascript.enabled') != false)
     {	  	  
       pref.set('javascript.enabled', false);
       if(reload)
       {      	     
           Main.reload();
       }
     }
   }	 
  },


  reload: function(){
    tabs.activeTab.reload(); 	   	  
  },
    
  add_to_whitelist: function() {
    Main.host =  url.URL(tabs.activeTab.url).host;
    ss.storage.whitelist.push(Main.host);
    ss.storage.whitelist = Main.uniques(ss.storage.whitelist);
   },

  remove_from_whitelist: function() {
    Main.host =  url.URL(tabs.activeTab.url).host;
    if(Main.is_in_whitelist())
    {
      var index = ss.storage.whitelist.indexOf(Main.host);	  
      ss.storage.whitelist.splice(index,1);
    }
   },

  is_in_whitelist: function()
  {
    var index = ss.storage.whitelist.indexOf(Main.host);
    if(index > -1)
    {
      return true;
    }
    else
    {
      return false;
    }      
  },

  set_button_state: function()
  {
    if(pref.get('javascript.enabled') == true)
    {
      Main.button.state(Main.button, Main.white_state);
    }
    else
    {
      Main.button.state(Main.button, Main.black_state);	  
    }	  
  },

  uniques: function(arr) {
    var a = [];
    for (var i=0, l=arr.length; i<l; i++)
    {
      if (a.indexOf(arr[i]) === -1 && arr[i] !== '')
      {
        a.push(arr[i]);
      }
    }
    return a;
  },

  add_list_to_whitelist: function(list) {
    var lines = list.match(/^.*((\r\n|\n|\r)|$)/gm);
    for(i in lines)
    {
      var host = lines[i].trim();	
      if(Main.is_host_name(host))
      {
        ss.storage.whitelist.push(host);
      }	  
    }
    ss.storage.whitelist = Main.uniques(ss.storage.whitelist);	  
  },
  save_whitelist: function(name,str) {
    var filename = path.join(desktop_path, name);    
    var text_writer = file.open(filename, 'w');
    text_writer.write(str);
    text_writer.close();	
  },
    
  read_whitelist: function(filename) {
    if(!file.exists(filename))
    {
      return null;
    }
    var text_reader = file.open(filename, 'r');
    var str = text_reader.read();
    text_reader.close();
    return str;
  },
  is_host_name: function(string) {
    if (/^((2[0-4]|1[0-9]|[1-9])?[0-9]|25[0-5])(\.((2[0-4]|1[0-9]|[1-9])?[0-9]|25[0-5])){3}$/.test(string))
    {
      var octets = string.split('.');
      if ((octets[0] == 0) || (octets[0] == 10) || (octets[0] == 127) || (octets[3] == 255)
  	|| (octets[3] == 0)
	|| ((octets[0] == 169) && (octets[1] == 254))
	|| ((octets[0] == 172) && (octets[1] & 0xf0 == 16))
	|| ((octets[0] == 192) && (octets[1] == 0) && (octets[2] == 2))
	|| ((octets[0] == 192) && (octets[1] == 88) && (octets[3] == 99))
	|| ((octets[0] == 192) && (octets[1] == 168))
	|| ((octets[0] == 198) && (octets[1] & 0xfe == 18))
	|| (octets[0] & 0xf0 == 224)
	|| (octets[0] & 0xf0 == 240))
  	return false;
    }
    else if (!/^([a-z0-9][a-z0-9-]*[a-z0-9]\.)+[a-z]{2,}$/.test(string)) return false;
    return true;
  }	
};



panel.port.on('remove_domains', function handleMyMessage(rm)
{
  for(i in rm)
  {
    var domain = rm[i];
    var index = ss.storage.whitelist.indexOf(domain);
    if(index > -1)
    {
      ss.storage.whitelist.splice(index,1);
    }
  }
});


sp.on("edit_whitelist", function()
{
  panel.show();
  panel.port.emit("fill_list",ss.storage.whitelist);
});


sp.on("export_whitelist", function()
{
  var text = ss.storage.whitelist.sort().join('\n');
  Main.save_whitelist('javascript_whitelist.txt',text);
  msg.alert(null,Main.add_on_name, 'Whitelist successfully saved to your Desktop!');    
});


sp.on("import_whitelist", function(e)
{
  var text = Main.read_whitelist(prefs["import_whitelist"]);
  Main.add_list_to_whitelist(text);
  msg.alert(null,Main.add_on_name, 'Whitelist successfully added!');
});


exports.onUnload = function(e)
{
  if(e == 'uninstall' || e ==='disable' || e === 'shutdown')
  {
    pref.set('javascript.enabled', true);
  }

  if(e == 'uninstall')
  {
    delete ss.storage.whitelist;
  }

}

exports.main = function(e)
{
  pref.set('javascript.enabled', false);
}

Main.init();





