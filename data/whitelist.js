var select     = document.getElementById("select");
var remove     = document.getElementById("remove");
var remove_all = document.getElementById("remove_all");
remove.disabled = true

remove_all.addEventListener('click', function onkeyup(event) {
  for(var x=0;x< select.options.length;x++)
  {
    select.options[x].selected = true
  }
  remove_selections();
}, false);

remove.addEventListener('click', function onkeyup(event) {
  remove_selections();
}, false);

select.addEventListener('click', function onkeyup(event) {
  remove.disabled = false
}, false);

self.port.on('fill_list', function(a) {
  refill_selections(a);
});


function remove_selections()
{
  var rm     = [];    
  var remain = [];    
  for(var x=0;x< select.options.length;x++)
  {
    if(select.options[x].selected == true)
    {
      rm.push(select.options[x].innerHTML);	  
    }
    else
    {
      remain.push(select.options[x].innerHTML);
    }
  }
  refill_selections(remain);
  self.port.emit('remove_domains',rm);
}


function refill_selections(a)
{
  select.innerHTML = "";
  for(i in a)
  {
   var opt = document.createElement("option");
   opt.value= i;
   var text = document.createTextNode(a[i]);
   opt.appendChild(text);      
   select.appendChild(opt);
  }
}
